﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnterpriseV2.Models;
using System;

namespace EnterpriseV2.Controllers
{
    [Route("api/enterprise")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private readonly EnterpriseContext _contextE;

        public EnterpriseController(EnterpriseContext context)
        {
            _contextE = context;
            //Inclui alguns campos na TypeEnterprises para fins de teste.
            if (_contextE.Enterprises.Count() == 0)
            {
                for (int i = 1; i < 6; i++)
                {
                    _contextE.TypeEnterprises.Add(new TypeEnterprise { Name = ("Agro é pop"+i.ToString()) });
                    _contextE.SaveChanges();
                }
                for (int i = 1; i < 6; i++)
                {
                    var todoItem = _contextE.TypeEnterprises.FindAsync((long)i);
                    _contextE.Enterprises.Add(new Enterprise { EmailEnterprise = "pereke007@gmail.com", City = "Lavras", EnterpriseName = ("Cantina" + i.ToString()), Description = "Lanchao do bom", Country = "Brasil", Linkedin = "", OwnEnterprise = false, Phone = "", Value = 0, Photo = "", SharePrice = 5000, Twitter = "", Facebook = "", EnterpriseType = todoItem.Result });
                    _contextE.SaveChanges();
                }
                

            }
            
        }

        // GET: api/enterprise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetEnterprises()
        {
            _contextE.TypeEnterprises.ToList();
            var aux =  await _contextE.Enterprises.ToListAsync();
            return aux;
        }

        // GET: api/enterprise/1
        // Mostra a enterprise com o id passado
        [HttpGet("{id}")]
        public async Task<ActionResult<Enterprise>> GetEnterprise(int id)
        {
            _contextE.TypeEnterprises.ToList();
            await _contextE.Enterprises.FindAsync(id);
            var todoItem = await _contextE.Enterprises.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        //Mostra a enterprise que contem no minimo parte do nome passado (Contais) e o id passado
        //GET api/enterprise/enterprise?id=1&name=cantina0
        [HttpGet("enterprise")]
        public async Task<List<Enterprise>> GetEnterprise(int id, string name)
        {
            _contextE.TypeEnterprises.ToList();
            Console.WriteLine(_contextE.TypeEnterprises.Any());
            var todoItem = await _contextE.Enterprises.Where(x => ((x.EnterpriseType.Id == id) && (x.EnterpriseName.Contains(name)))).ToListAsync();
            if (todoItem == null)
            {
                return null;
            }
            return todoItem;
        }
    }



}