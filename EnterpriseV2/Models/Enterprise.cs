// Unused usings removed
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EnterpriseV2.Models;

namespace EnterpriseV2.Models
{
    public class Enterprise
    {
        public int Id { get; set; }
        public string EmailEnterprise { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool OwnEnterprise { get; set; }
        public string EnterpriseName { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public float Value { get; set; }
        public float SharePrice { get; set; }
        public float Share { get; set; }
        public TypeEnterprise EnterpriseType { get; set; }

    }
}