// Unused usings removed
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EnterpriseV2.Models;

namespace EnterpriseV2.Models
{
    public class TypeEnterprise
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}