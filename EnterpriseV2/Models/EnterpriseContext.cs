using Microsoft.EntityFrameworkCore;

namespace EnterpriseV2.Models
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions<EnterpriseContext> options)
            : base(options)
        {
        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<TypeEnterprise> TypeEnterprises { get; set; }
    }
}